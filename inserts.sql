
-- Stadium Project
-- Imane & Guillaume
-- Requetes inserant des éléments de la fiche de Thomas d'Acquino dans la base

INSERT INTO voyages (id,personne_id,type_de_voyage,date_debut,date_fin,periode_approximative,motif,lieu,reference,commentaire,texte) VALUES (1,12000,'',1269,1272,'','','Paris, France','',NULL,NULL),
 (2,12000,NULL,1252,1259,'',NULL,'Paris',NULL,NULL,NULL),
 (3,12000,NULL,1252,1259,'',NULL,'Valencienne, France',NULL,NULL,NULL);

INSERT INTO universites_ou_studiums (id,personne_id,etablissement,date_debut,date_fin,periode_approximative,lieu,reference,commentaire,texte) VALUES (1,12000,'',1239,1245,NULL,'Naples',NULL,NULL,NULL),
 (2,12000,'Couvent (dominicain) de Paris  (Saint-Jacques)',1246,1248,NULL,'Paris','TORRELL, 40-47.','Ce premier séjour correspond à trois années scolaires ; selon Jean-Pierre TORRELL , il aurait terminé à Paris son étude des arts commencée à Naples et travaille en particulier sur l’Éthique à Nicomaque dont la translatio vetus est toujours en usage à Paris : il la citera toute da vie alors que son maître ALBERT le Grand connaît déjà à cette époque la translatio lincolniensis. Mais il commence en même temps sa formation en théologie en suivant les cours d’ ALBERT le Grand au couvent Saint-Jacques, devenant son assistant (c’est lui qui mettra au net ses cours sur la Hiérarchie céleste et le De divinis nominibus du pseudo-DENYS et sur l’Éthique à Nicomaque d’ ARISTOTE ) et le suivant quand il part à Cologne après que le chapitre général (dominicain) de Paris ait décidé en 1248 (7 juin, jour de la Pentecôte ) d’y créer un studium generale ; ',NULL);

INSERT INTO types_de_liens_familiaux (id,type) VALUES (1,'Père'),
 (2,'Mère');

INSERT INTO titres_des_oeuvres (id,titre,variante,texte) VALUES (1,'Principia','Commentatio Sacrae Scripturae',NULL);

INSERT INTO statuts (id,personne_id,statut,reference,commentaire,texte) VALUES (1,12000,'Maitre',NULL,NULL,NULL);

INSERT INTO productions_textuelle (id,personne_id,reference,commentaire,texte,indication) VALUES (1,12000,NULL,NULL,NULL,' Oeuvre écrite en latin.');

INSERT INTO polemiques (id,personne_id,sujet,reference,commentaire,texte) VALUES (1,12000,'Averroïsme latin',NULL,NULL,'Impliqué dans une polémique avec Gérard de ABBEVILLE et les maîtres séculiers sur l''averroïsme latin à Paris en 1269 -1272 . '),
 (2,12000,'',NULL,NULL,'Impliqué dans une polémique avec Guillaume de SAINT-AMOUR , $Gérard d’ABBEVILLE , Nicolas de LISIEUX$ et les maitres séculiers à Paris en 1256 -1259 ; ');

INSERT INTO personnes (id,nom,prenom) VALUES (12000,'De Aquino','Thomas');

INSERT INTO periodes_activite (id,personne_id,date_debut,date_fin,periode_approximative,reference,commentaire,texte) VALUES (1,12000,1250,1274,NULL,NULL,NULL,NULL);

INSERT INTO origines (id,personne_id,pays,ville,reference,commentaire,texte) VALUES (1,12000,'Italie',' Roccasecca',NULL,'Le château se trouve Aquino , dans le province de Frosinone , à proximité de l’abbaye du Mont-Cassin , à la limite entre le royaume de Naples et les états du pape. ',NULL);

INSERT INTO oeuvres (id,champ,titre_id,auteur_id,reference,commentaire) VALUES (1,19,1,12000,NULL,'Le principium ‘Rigans montes de superioribus’ est celui qui a été tenu à l’occasion de son inception ; le second principium ‘Hic est liber mandatorum Dei’ pourrait avoir été prononcé lors de la resumptio, la première lecture suivant l’inception.');
INSERT INTO membres_de_la_famille (id,personne_id,type,nom,prenom,reference,commentaire,texte,complement) VALUES (1,12000,1,'d’Aquino','Landolfo','','','','Chevalier'),
 (2,12000,2,'Rossi',' Theodora',NULL,NULL,NULL,'De la famille Caracciolo');

INSERT INTO medianes_activite (id,personne_id,valeur_date,valeur_date_approximative,reference,commentaire,texte) VALUES (1,12000,1262,NULL,NULL,NULL,NULL);

INSERT INTO maitres (id,personne_id,nom,prenom,lieu,reference,commentaire,texte) VALUES (1,12000,'Albert','Le Grand','Paris',NULL,NULL,NULL),
 (2,12000,'Albert','Le Grand','Cologne',NULL,NULL,NULL);

INSERT INTO lieux_des_oeuvres (id,oeuvre_id,lieu,indication,texte,reference,commentaire) VALUES (1,1,'Paris','Lieu de publication','','TORRELL 446-447.','');

INSERT INTO formations (id,personne_id,categorie_id,titre,date_debut,date_fin,periode_approximative,lieu,reference,commentaire,texte,sous_categorie) VALUES (1,12000,1,'Oblat',1230,1236,'','Abbaye (bénédictine) du Mont-Cassin','','','',NULL),
 (2,12000,1,'','','','','Naples','TORRELL, 24-25.','Chassé du Mont-Cassin par la guerre, il réside sans doute au monastère (bénédictin) de Naples (San Demetrio) ; ',NULL,NULL);

INSERT INTO descriptifs (id,personne_id,descriptif_court,reference,commentaire,texte) VALUES (1,12000,' Dominicain',NULL,NULL,NULL);

INSERT INTO dates_des_oeuvres (id,oeuvre_id,valeur_date,valeur_date_approximative,indication,texte,reference,commentaire) VALUES (1,1,1256,'Mars-Juin','Date de publication',NULL,NULL,NULL);

INSERT INTO dates_de_naissance (id,personne_id,valeur_date,valeur_date_approximative,reference,commentaire,texte) VALUES (1,12000,'','Vers 1226',NULL,NULL,NULL);

INSERT INTO dates_de_mort (id,personne_id,valeur_date,valeur_date_approximative,reference,commentaire,texte) VALUES (1,12000,1274,'','TORRELL, 369-376.',' Il meurt à Fossanova en 1274 (7 mars) alors qu’il était en route pour participer au concile de Lyon : mais il était malade depuis décembre 1273 (peut-être un AVC subi pendant une messe qu’il célèbre aux environs du 6 décembre ;',NULL);

INSERT INTO cursus (id,personne_id,designation,date_obtention,periode_approximative,lieu,reference,commentaire,texte) VALUES (1,12000,'Bachelier biblique en théologie',NULL,'1252 -1254','Paris',NULL,NULL,'Bachelier biblique en théologie ( Paris ) 1252 -1254 .'),
 (2,12000,'Maître en théologie ',1256,'','Paris','TORRELL, 81-85.','Il tient sa leçon inaugurale en 1256 (entre le 3 mars et le 17 juin) ;<5c> Bachelier en théologie ( Paris ) 1254 -1256',NULL);
