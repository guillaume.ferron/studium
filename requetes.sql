
-- Ramener toutes les oeuvres d'un auteur
SELECT personnes.nom,personnes.prenom,titres_des_oeuvres.titre
FROM personnes
INNER JOIN oeuvres ON oeuvres.auteur_id = personnes.id
INNER JOIN titres_des_oeuvres ON titres_des_oeuvres.id = oeuvres.titre_id;

-- Trouver les personnes qui ont voyagé à Paris
SELECT *
FROM personnes
LEFT JOIN voyages
WHERE voyages.lieu LIKE '%Paris%';

-- Rapatrier le prénom de la mère d'une personne
SELECT membres_de_la_famille.prenom
FROM membres_de_la_famille
INNER JOIN personnes
WHERE personnes.id=12000
AND membres_de_la_famille.type = 2;


-- selectionner tous les commentaires et références d'une personne à travers plusieurs tables
SELECT reference,commentaire
FROM universites_ou_studiums
WHERE universites_ou_studiums.personne_id = 12000
AND reference IS NOT NULL
UNION
SELECT reference,commentaire
FROM cursus
WHERE cursus.personne_id = 12000
AND reference IS NOT NULL
UNION
SELECT reference,commentaire
FROM formations
WHERE formations.personne_id = 12000
AND reference IS NOT NULL;


