-- Stadium Project
-- Imane & Guillaume

-- --------------------------------------------------
--   TODO
-- --------------------------------------------------
-- Eventuellement remplacer les champs de type TEXT par des champs de type VARCHAR(1000) (ou 2000, ou 4000) afin de mieux respecter le standard SQL.
-- Créer une fonction SQL afin de vérifier qu'à l'insertion de chaines de caractères toutes les entrées sont au format "Ma nouvelle entrée (en termes de majuscules, en utilisant un mix de UPPER, LOWER, une boucle.
-- Créer une fonction et un TRIGGER SQL afin de vérifier qu'à l'insertion un nom alternatif ne peut être entré si le même nom existe déjà dans les colonnes nom / prenom de la table personnes
-- Créer une fonction et un TRIGGER SQL afin de vérifier à l'insertion qu'une entrée valeur_date dans la table date_de_mort est toujours postérieure à la valeur_date dans la table date_de_naissance, pour une personne donnée. Pour les dates approximatives on ne peut rien faire facilement sans risque d'erreur.
-- Créer une fonction pour SQL afin de vérifier à l'insertion qu'une entrée date_debut est toujours postérieure à la date_fin de la même table.
-- Créer une fonction SQL afin de vérifier qu'à l'insertion tous les champs TEXT et VARCHAR(255) se terminent par un . (point)
-- Etudier plus en profondeur l'usage puis, si c'est pertinent, créer une fonction et un TRIGGER visant à renseigner automatiquement une nouvelle ligne dans la table mediane_d'activite dès qu'une entrée apparait dans la table periodes_activite.
-- Repenser le modèle afin de le rendre soit plus simple à la saisie, soit plus optimisé en termes de rangement. Faire un choix.

-- --------------------------------------------------
--     NOTE
-- --------------------------------------------------
-- Les champs "texte" présents presque systématiquement servent à héberger le contenu actuel de la base de donnée, en vue de le refactorer plus tard et de procéder à l'amélioration de la base de manière incrémentale.

-- --------------------------------------------------
--    SCRIPT
-- --------------------------------------------------


DROP DATABASE IF EXISTS stadium; -- Testing purpose
CREATE DATABASE stadium;
use stadium;

-- Creation de la table des personnes
-- Cette table référence les différentes personnes qui seront ensuite présentées sur le site Web http://lamop-vs3.univ-paris1.fr/studium
CREATE TABLE personnes (
   id INTEGER NOT NULL,
   nom VARCHAR(35),
   prenom VARCHAR(35),
   PRIMARY KEY (id)
);

-- Creation de la table des descriptions liées aux personnes
-- Cette table permet de constituer une rubrique "description courte"
CREATE TABLE descriptifs (
   id INTEGER NOT NULL,
   personne_id INTEGER NOT NULL,
   descriptif_court VARCHAR(255),
   reference VARCHAR(255),
   commentaire VARCHAR(255),
   texte TEXT,
   PRIMARY KEY (id),
   FOREIGN KEY (personne_id) REFERENCES personnes (id)
);

-- Creation de la table des éventuels noms alternatifs des personnes
-- Cette table permet de constituer une rubrique "variante du nom"
CREATE TABLE identites (
   id INTEGER NOT NULL,
   personne_id INTEGER NOT NULL,
   nom VARCHAR(35),
   prenom VARCHAR(35),
   reference VARCHAR(255),
   commentaire VARCHAR(255),
   texte TEXT,
   PRIMARY KEY (id),
   FOREIGN KEY (personne_id) REFERENCES personnes (id)
);

-- Creation de la table des universités ou studiuams fréquentées par les personnes
-- Cette table permet de constituer une rubrique "universités ou studiums fréquentés"
CREATE TABLE universites_ou_studiums (
  id INTEGER NOT NULL,
  personne_id INTEGER NOT NULL,
  etablissement VARCHAR(35),
  date_debut DATE,
  date_fin DATE,
  periode_approximative VARCHAR(25),
  lieu VARCHAR(35),
  reference VARCHAR(255),
  commentaire VARCHAR(255),
  texte TEXT,
  PRIMARY KEY(id),
  FOREIGN KEY(personne_id) REFERENCES personnes(id)
);

-- Creation de la table qui regroupe les dates de naissance les personnes
-- Cette table permet de constituer une rubrique "date de naissance"
CREATE TABLE dates_de_naissance (
   id INTEGER NOT NULL,
   personne_id INTEGER NOT NULL,
   valeur_date DATE,
   valeur_date_approximative VARCHAR(15),
   reference VARCHAR(255),
   commentaire VARCHAR(255),
   texte TEXT,
   PRIMARY KEY (id),
   FOREIGN KEY (personne_id) REFERENCES personnes (id)
);

-- Creation de la table qui regroupe les dates de décès les personnes
-- Cette table permet de constituer une rubrique "date de décès"
CREATE TABLE dates_de_mort (
   id INTEGER NOT NULL,
   personne_id INTEGER NOT NULL,
   valeur_date DATE,
   valeur_date_approximative VARCHAR(15),
   reference VARCHAR(255),
   commentaire VARCHAR(255),
   texte TEXT,
   PRIMARY KEY (id),
   FOREIGN KEY (personne_id) REFERENCES personnes (id)
);

-- Creation de la table qui regroupe les périodes d'activité des personnes
-- Cette table permet de constituer une rubrique "période d'activité"
-- Sa structure permet, en cas de besoin, de définir plusieurs périodes d'activité afin d'être en mesure de rendre compte d'une carrière riche entrecoupée de creux.
CREATE TABLE periodes_activite (
   id INTEGER NOT NULL,
   personne_id INTEGER NOT NULL,
   date_debut DATE,
   date_fin DATE,
   periode_approximative VARCHAR(25),
   reference VARCHAR(255),
   commentaire VARCHAR(255),
   texte TEXT,
   PRIMARY KEY (id),
   FOREIGN KEY (personne_id) REFERENCES personnes (id)
);

-- Creation de la table qui regroupe les médianes d'activité des personnes
-- Cette table permet de constituer une rubrique "médiane d'activité"
CREATE TABLE medianes_activite (
   id INTEGER NOT NULL,
   personne_id INTEGER NOT NULL,
   valeur_date DATE,
   valeur_date_approximative VARCHAR(15),
   reference VARCHAR(255),
   commentaire VARCHAR(255),
   texte TEXT,
   PRIMARY KEY (id),
   FOREIGN KEY (personne_id) REFERENCES personnes (id)
);

-- Creation de la table qui regroupe le statut, "maitre" ou élève"
-- Cette table permet de constituer une rubrique "statut"
CREATE TABLE statuts (
   id INTEGER NOT NULL,
   personne_id INTEGER NOT NULL,
   statut VARCHAR(35), 
   reference VARCHAR(255),
   commentaire VARCHAR(255),
   texte TEXT,
   PRIMARY KEY (id),
   FOREIGN KEY (personne_id) REFERENCES personnes (id),
   CHECK (statut IN ('Maitre', 'Eleve'))
);

-- Creation de la table qui regroupe l'origine géographique des personnes
-- Cette table permet de constituer une rubrique "origine"
CREATE TABLE origines (
   id INTEGER NOT NULL,
   personne_id INTEGER NOT NULL,
   pays VARCHAR(35),
   ville VARCHAR(35),
   reference VARCHAR(255),
   commentaire VARCHAR(255),
   texte TEXT,
   PRIMARY KEY (id),
   FOREIGN KEY (personne_id) REFERENCES personnes (id)
);

-- Creation de la table qui regroupe le type de liens familiaux qui peuvent exister entre deux personnes
-- Cette table permet de constituer une rubrique "réseau familial"
CREATE TABLE types_de_liens_familiaux (
   id INTEGER,
   type VARCHAR(35),
   PRIMARY KEY (id)
);

-- Creation de la table qui regroupe les membres de la famille des personnes
-- Cette table permet de constituer une rubrique "réseau familial"
-- Le type est contraint par les entrées de la table types_de_liens_familiaux
CREATE TABLE membres_de_la_famille (
   id INTEGER NOT NULL,
   personne_id INTEGER NOT NULL,
   type INTEGER,
   nom VARCHAR(35),
   prenom VARCHAR(35),
   reference VARCHAR(255),
   commentaire VARCHAR(255),
   texte TEXT, complement VARCHAR(100),
   PRIMARY KEY (id),
   FOREIGN KEY (personne_id) REFERENCES personnes (id),
   FOREIGN KEY (type) REFERENCES types_de_liens_familiaux (id)
);

-- Creation de la table qui regroupe les différentes polémiques pouvant être liées à des personnes référencées dans la base
-- Cette table permet de constituer une rubrique "polémiques"
CREATE TABLE polemiques (
   id INTEGER NOT NULL,
   personne_id INTEGER NOT NULL,
   sujet VARCHAR(100),
   reference VARCHAR(255),
   commentaire VARCHAR(255),
   texte TEXT,
   PRIMARY KEY (id),
   FOREIGN KEY (personne_id) REFERENCES personnes (id)
);

-- Creation de la table qui regroupe les apprenants des personnes
-- Cette table permet de constituer une rubrique "lien maitre-élève"
CREATE TABLE maitres (
   id INTEGER NOT NULL,
   personne_id INTEGER NOT NULL,
   nom VARCHAR(35),
   prenom VARCHAR(35),
   lieu VARCHAR(35),
   reference VARCHAR(255),
   commentaire VARCHAR(255),
   texte TEXT,
   PRIMARY KEY (id),
   FOREIGN KEY (personne_id) REFERENCES personnes (id)
);

-- Creation de la table qui regroupe les apprentis des personnes
-- Cette table permet de constituer une rubrique "lien maitre-élève"
CREATE TABLE eleves (
   id INTEGER NOT NULL,
   personne_id INTEGER NOT NULL,
   nom VARCHAR(35),
   prenom VARCHAR(35),
   lieu VARCHAR(35),
   reference VARCHAR(255),
   commentaire VARCHAR(255),
   texte TEXT,
   PRIMARY KEY (id),
   FOREIGN KEY (personne_id) REFERENCES personnes (id)
);


-- Creation de la table qui regroupe les catégories utilisées par de nombreuses autres tables dans la base
CREATE TABLE categories (
  id INTEGER NOT NULL,
  nom VARCHAR(35),
  PRIMARY KEY(id)
);

-- Creation de la table qui regroupe les formations suivies par les personnes
-- Cette table permet de constituer une rubrique comme "formations pré-universitaire" et d'autres types de formation en fonction de la "catégorie_id" renseignée
CREATE TABLE formations (
  id INTEGER NOT NULL,
  personne_id INTEGER NOT NULL,
  categorie_id INTEGER NOT NULL,
  titre VARCHAR(35),
  date_debut DATE,
  date_fin DATE,
  periode_approximative VARCHAR(25),
  lieu VARCHAR(35),
  reference VARCHAR(255),
  commentaire VARCHAR(255),
  texte TEXT,
  sous_categorie INTEGER,
  PRIMARY KEY(id),
  FOREIGN KEY(personne_id) REFERENCES personnes(id),
  FOREIGN KEY(categorie_id) REFERENCES categories(id),
  FOREIGN KEY(sous_categorie) REFERENCES categories(id)
);

-- Creation de la table qui regroupe les fonctions occupées par des personnes dans un secteur donné, selon ce qui est renseigné dans le champ "categorie".
-- Cette table permet de constituer une rubrique "Carrières".
CREATE TABLE carrieres (
  id INTEGER NOT NULL,
  personne_id INTEGER NOT NULL,
  categorie_id INTEGER NOT NULL,
  sous_categorie_id INTEGER,
  fonction_ou_raison VARCHAR(35),
  date_debut DATE,
  date_fin DATE,
  periode_approximative VARCHAR(25),
  lieu VARCHAR(255),
  reference VARCHAR(255),
  commentaire VARCHAR(255),
  texte TEXT,
  PRIMARY KEY(id),
  FOREIGN KEY(personne_id) REFERENCES personnes(id),
  FOREIGN KEY(categorie_id) REFERENCES categories(id),
  FOREIGN KEY(sous_categorie_id) REFERENCES categories(id)
);


-- Creation de la table qui regroupe le parcours diplomant des personnes.
-- Cette table permet de constituer une rubrique "Cursus".
CREATE TABLE cursus (
  id INTEGER NOT NULL,
  personne_id INTEGER NOT NULL,
  designation VARCHAR(35),
  date_obtention DATE,
  periode_approximative VARCHAR(25),
  lieu VARCHAR(35),
  reference VARCHAR(255),
  commentaire VARCHAR(255),
  texte TEXT,
  PRIMARY KEY(id),
  FOREIGN KEY(personne_id) REFERENCES personnes(id)
);

-- Creation de la table qui regroupe les déplacements à l'étranger des personnes.
-- Cette table permet de constituer une rubrique "Voyages".
-- Remarque : le "type_de_voyage" correspond en réalité à une "categorie_id" et il est optionnel.
CREATE TABLE voyages (
   id INTEGER NOT NULL,
   personne_id INTEGER NOT NULL,
   type_de_voyage INTEGER,
   date_debut DATE,
   date_fin DATE,
   periode_approximative VARCHAR(25),
   motif VARCHAR(35),
   lieu VARCHAR(35),
   reference VARCHAR(255),
   commentaire VARCHAR(255),
   texte TEXT,
   PRIMARY KEY (id),
   FOREIGN KEY (personne_id) REFERENCES personnes (id),
   FOREIGN KEY (type_de_voyage) REFERENCES categories (id)
);

-- Creation de la table qui regroupe les compétences principales des personnes.
-- Cette table permet de constituer une rubrique "Commission/Expertise".
-- La categorie_id est optionnelle.
CREATE TABLE expertises (
  id INTEGER NOT NULL,
  personne_id INTEGER NOT NULL,
  categorie_id INTEGER,
  sujet VARCHAR(100),
  niveau VARCHAR(35),
  reference VARCHAR(255),
  commentaire VARCHAR(255),
  texte TEXT,
  sous_categories_id INTEGER,
  PRIMARY KEY(id),
  FOREIGN KEY(personne_id) REFERENCES personnes(id),
  FOREIGN KEY(categorie_id) REFERENCES categories(id),
  FOREIGN KEY(sous_categories_id) REFERENCES categories(id)
);

-- Creation de la table qui regroupe les groupements et commissions auxquels des personnes ont participé dans le cadre de leur expertise.
-- Cette table permet de constituer une rubrique "Commission/Expertise".
-- La categorie_id est optionnelle.
CREATE TABLE commissions (
  id INTEGER NOT NULL,
  personne_id INTEGER NOT NULL,
  categorie_id INTEGER,
  sous_categorie INTEGER,
  nom VARCHAR(35),
  role VARCHAR(35),
  objet VARCHAR(35),
  date_debut DATE,
  date_fin DATE,
  periode_approximative VARCHAR(25),
  reference VARCHAR(255),
  commentaire VARCHAR(255),
  texte TEXT,
  PRIMARY KEY(id),
  FOREIGN KEY(personne_id) REFERENCES personnes(id),
  FOREIGN KEY(categorie_id) REFERENCES categories(id),
  FOREIGN KEY(sous_categorie) REFERENCES categories(id)
);

-- Creation de la table qui regroupe des indications libres décrivant la production littéraire des personnes
-- Cette table permet de constituer une rubrique "production textuelle"
CREATE TABLE productions_textuelle (
  id INTEGER NOT NULL,
  personne_id INTEGER NOT NULL,
  reference VARCHAR(255),
  commentaire VARCHAR(255),
  texte TEXT,
  indication VARCHAR(255),
  PRIMARY KEY(id),
  FOREIGN KEY(personne_id) REFERENCES personnes(id)
);

-- Creation de la table qui regroupe les titres des oeuvres des personnes
-- Cette table permet de constituer une rubrique "oeuvres"
-- La "categorie_id" permet de définir le champ de l'oeuvre.
CREATE TABLE titres_des_oeuvres (
  id INTEGER NOT NULL,
  titre VARCHAR(255),
  variante VARCHAR(255),
  texte TEXT,
  PRIMARY KEY(id)
);

-- Creation de la table qui regroupe les oeuvres des personnes
-- Cette table permet de constituer une rubrique "oeuvres"
CREATE TABLE oeuvres (
  id INTEGER NOT NULL,
  champ INTEGER,
  titre_id INTEGER NOT NULL,
  auteur_id INTEGER,
  reference VARCHAR(255),
  commentaire VARCHAR(255),
  PRIMARY KEY(id),
  FOREIGN KEY(titre_id) REFERENCES titres_des_oeuvres(id),
  FOREIGN KEY(auteur_id) REFERENCES personnes(id)
);

-- Creation de la table qui regroupe les dates des oeuvres des personnes
-- L'indication sert à préciser en quoi la date concerne l'oeuvre
-- Cette table permet de constituer une rubrique "oeuvres"
CREATE TABLE dates_des_oeuvres (
  id INTEGER NOT NULL,
  oeuvre_id INTEGER,
  valeur_date DATE,
  valeur_date_approximative VARCHAR(15),
  indication VARCHAR(35),
  texte TEXT,
  reference VARCHAR(255),
  commentaire INTEGER,
  PRIMARY KEY(id),
  FOREIGN KEY(oeuvre_id) REFERENCES oeuvres(id)
);

-- Creation de la table qui regroupe les éléments qui composent les oeuvres des personnes (manuscrits, incipits, etc.)
-- Cette table permet de constituer une rubrique "oeuvres"
CREATE TABLE composition_des_oeuvres (
  id INTEGER NOT NULL,
  oeuvre_id INTEGER,
  type INTEGER,
  titre VARCHAR(255),
  commentaire INTEGER,
  reference INTEGER,
  PRIMARY KEY(id),
  FOREIGN KEY(oeuvre_id) REFERENCES oeuvres(id),
  FOREIGN KEY(type) REFERENCES categories(id)
);

-- Creation de la table qui regroupe les lieux liées aux oeuvres des personnes
-- Cette table permet de constituer une rubrique "oeuvres"
CREATE TABLE lieux_des_oeuvres (
  id INTEGER NOT NULL,
  oeuvre_id INTEGER,
  lieu VARCHAR(255),
  indication VARCHAR(35),
  texte TEXT,
  reference VARCHAR(255),
  commentaire VARCHAR(255),
  PRIMARY KEY(id),
  FOREIGN KEY(oeuvre_id) REFERENCES oeuvres(id)
);


-- Creation de la table qui regroupe les références bibliographiques qui ont permit d'inclure des personnes dans la base
-- Cette table permet de constituer une rubrique "bibliographie"
CREATE TABLE bibliographie (
  id INTEGER NOT NULL,
  personne_id INTEGER,
  type INTEGER,
  reference_bibliographique VARCHAR(255),
  lien_web INTEGER,
  PRIMARY KEY(id),
  FOREIGN KEY(personne_id) REFERENCES personnes(id),
  FOREIGN KEY(type) REFERENCES categories(id)
);

